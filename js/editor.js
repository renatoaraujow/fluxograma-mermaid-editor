const elementFunctions = [];
let index = 1;
let elementClicked;
let elementClickedPositionX;
let elementClickedPositionY;
let timer;
let fatherOptions = [];

function insertAtCaret(areaId, text) {
  var txtarea = document.getElementById(areaId);
  if (!txtarea) {
    return;
  }

  var scrollPos = txtarea.scrollTop;
  var strPos = 0;
  var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
    "ff" : (document.selection ? "ie" : false));
  if (br == "ie") {
    txtarea.focus();
    var range = document.selection.createRange();
    range.moveStart('character', -txtarea.value.length);
    strPos = range.text.length;
  } else if (br == "ff") {
    strPos = txtarea.selectionStart;
  }

  var front = (txtarea.value).substring(0, strPos);
  var back = (txtarea.value).substring(strPos, txtarea.value.length);
  txtarea.value = front + text + back;
  strPos = strPos + text.length;
  if (br == "ie") {
    txtarea.focus();
    var ieRange = document.selection.createRange();
    ieRange.moveStart('character', -txtarea.value.length);
    ieRange.moveStart('character', strPos);
    ieRange.moveEnd('character', 0);
    ieRange.select();
  } else if (br == "ff") {
    txtarea.selectionStart = strPos;
    txtarea.selectionEnd = strPos;
    txtarea.focus();
  }

  txtarea.scrollTop = scrollPos;
}

function breakLine(textWithoutBreaks, numberCharacters) {
  if (textWithoutBreaks.length > numberCharacters) {
    const letters = textWithoutBreaks.split("");

    const brsLength = letters.length / numberCharacters;
    const lettersWithBr = [];
    let position = 1;
    let index = 0;

    letters.forEach((letra) => {
      if (position >= numberCharacters && letra === " " && brsLength > index) {
        lettersWithBr.push("&lt;br&gt;");
        index += 1;
        position = 0;
      } else {
        lettersWithBr.push(letra);
      }

      position += 1;
    });

    return lettersWithBr.join("");
  }

  return textWithoutBreaks;
}

function showTextBox(
  action,
  childType,
  elementName,
  elementId,
  elementClass,
  defaultValue = "",
  elementIdHtml,
  haveMoreInformation = false
) {
  
  if (action === "addFather") { 
    let optionsList = document.getElementById("select-father").options;

    fatherOptions.forEach((option) => { optionsList.add(new Option(option)) });

    $(".element-text-adicionar-pai").fadeIn(200).find("textarea").val(defaultValue).focus();
  } else {
    $(".element-text").fadeIn(200).find("textarea").val(defaultValue).focus();
  }

  $(".element-text .send-element-text")
    .attr("old-value", $(".element-text").find("textarea").val())
    .attr("action", action)
    .attr("option-type", childType)
    .attr("option-element-id", elementId)
    .attr("option-element-name", elementName)
    .attr("option-element-class", elementClass)
    .attr("option-element-id-html", elementIdHtml);

  if (action === "update" && haveMoreInformation) {
    $("#add-more-info").prop("checked", true);
    $(".element-text")
      .find("textarea")
      .val($(".element-text").find("textarea").val().replace("ⓘ", ""));
    
    for (const elementFunction of elementFunctions) {
      if (elementFunction.includes(elementName)) {
        $("#more-info-id")
          .val(elementFunction.match(/\(([^)]+)\)/)[1])
          .fadeIn(200);
      }
    }
  }
}

function showOptions(textToAppend) {
  $(".options-dropdown ul").empty();
  $(".options-dropdown")
    .css({ top: elementClickedPositionY, left: elementClickedPositionX })
    .fadeIn(500)
    .find("ul")
    .append(textToAppend);
}

function buildOptionCreate() {
  console.log('clicou aqui');
  let elementClassName = elementClicked
    .attr("class")
    .split(" ")
    .find((x) => x.includes("option"));
  const elementName = elementClicked.attr("id").split("-")[1];
  const elementNumber = elementName.replace(/[^0-9]/gi, "");

  if (elementClassName == undefined) {
    elementClassName = "";
  } else {
    elementClassName = `:::${elementClassName}`;
  }

  if (
    elementName.includes("C") ||
    elementName.includes("RS") ||
    elementName.includes("RN") ||
    elementName.includes("R")
  ) {
    showOptions(`<li><a href='javascript:buildOptionCreateLosango()' class='build-option' id='losango|${elementName}|${elementNumber}|${elementClassName}'>LOSANGO</a></li>
                <li><a href='#' class='build-option' id='retangulo|${elementName}|${elementNumber}|${elementClassName}'>RETANGULO</a></li>`);
  }
}

function buildOptionCreateLosango() {
  console.log('clicou aqui no create losango');
  let elementClassName = elementClicked
    .attr("class")
    .split(" ")
    .find((x) => x.includes("option"));
  const elementName = elementClicked.attr("id").split("-")[1];
  const elementNumber = elementName.replace(/[^0-9]/gi, "");

  if (elementClassName == undefined) {
    elementClassName = "";
  } else {
    elementClassName = `:::${elementClassName}`;
  }

  if (
    elementName.includes("C") ||
    elementName.includes("RS") ||
    elementName.includes("RN") ||
    elementName.includes("R")
  ) {
    showOptions(`<li><a href='javascript:buildOptionCreateLosango()' class='build-option' id='losango|${elementName}|${elementNumber}|${elementClassName}'>LOSANGO</a></li>
                <li><a href='#' class='build-option' id='retangulo|${elementName}|${elementNumber}|${elementClassName}'>RETANGULO</a></li>`);
  }
}

function buildOptionAddFather() {
  console.log('clicou aqui no adicionar pai');
  // let elementClassName = elementClicked
  //   .attr("class")
  //   .split(" ")
  //   .find((x) => x.includes("option"));
  // const elementName = elementClicked.attr("id").split("-")[1];
  // const elementNumber = elementName.replace(/[^0-9]/gi, "");

  // if (elementClassName == undefined) {
  //   elementClassName = "";
  // } else {
  //   elementClassName = `:::${elementClassName}`;
  // }

  // if (
  //   elementName.includes("C") ||
  //   elementName.includes("RS") ||
  //   elementName.includes("RN") ||
  //   elementName.includes("R")
  // ) {
  //   showOptions(`<li><a href='javascript:buildOptionCreateLosango()' class='build-option' id='losango|${elementName}|${elementNumber}|${elementClassName}'>LOSANGO</a></li>
  //               <li><a href='#' class='build-option' id='retangulo|${elementName}|${elementNumber}|${elementClassName}'>RETANGULO</a></li>`);
  // }
}

function buildOptionUpdate() {
  const elementId = elementClicked.attr("id");
  const elementClassName = elementClicked.attr("class").split(" ").find((x) => x.includes("option"));
  const elementName = elementClicked.attr("id").split("-")[1];
  const elementNumber = elementName.replace(/[^0-9]/gi, "");
  let nodeType = "";

  if (elementName.includes("C")) {
    nodeType = "circulo";
  } else if (
    elementName.includes("R") &&
    (!elementName.includes("RS") || !elementName.includes("RN"))
  ) {
    nodeType = "retangulo";
  } else if (elementName.includes("L")) {
    nodeType = "losango";
  }

  let elementTextPlain = $(`#${elementId}`).find("div").clone(true);
  elementTextPlain.find("span.mais-informacoes").remove();
  elementTextPlain = elementTextPlain.html().replace(/<br[^>]*>/g, "\n").replace(/<\/?[^>]+(>|$)/g, "").replace(/\n$/, "").trim();

  let haveMoreInformation = false;
  if ($(`#${elementId}`).find("div").text().includes("ⓘ")) {
    haveMoreInformation = true;
  }

  showTextBox(
    "update",
    nodeType,
    elementName,
    elementNumber,
    elementClassName,
    elementTextPlain,
    elementId,
    haveMoreInformation
  );
}

function buildOptionsElement() {
  const elementName = elementClicked.attr("id").split("-")[1];
  const elementNumber = parseInt(elementName.replace(/[^0-9]/gi, ""));

  let options = `<li><a href='javascript:buildOptionCreate()' class='element-menu create'>CRIAR</a></li>`;

  if (!elementName.includes("RS") && !elementName.includes("RN")) {
    options = `${options}<li><a href='javascript:buildOptionUpdate()' class='element-menu update'>EDITAR</a></li>`;
  }

  if (
    (elementClicked.hasPartialAttr("endNode") ||
      (elementName.includes("L") &&
        $(`[id*=flowchart-RS${elementNumber + 1}]`).hasPartialAttr("endNode") &&
        $(`[id*=flowchart-RN${elementNumber + 2}]`).hasPartialAttr(
          "endNode"
        ))) &&
    !elementName.includes("C") &&
    !elementName.includes("RS") &&
    !elementName.includes("RN")
  ) {
    options = `${options}<li><a href='#' class='element-menu remove'>REMOVER</a></li>`;
  }

  if ((elementName.includes("L") || elementName.includes("R")) && (!elementName.includes("RS") && !elementName.includes("RN"))) {
    options = `${options}<li><a href='javascript:buildOptionAddFather()' class='build-option-add-pai element-menu addFather'>ADICIONAR PAI</a></li>`;
  }
  showOptions(options);
}

function addEventContextMenu() {
  $("g.nodes").on("contextmenu", "> g", function (e) {
    e.preventDefault();
    elementClicked = $(this);
    elementClickedPositionX = e.pageX;
    elementClickedPositionY = e.pageY;
    buildOptionsElement();
  });

  $(".options-dropdown").mouseleave(() => {
    $(".options-dropdown").fadeOut(500).find("ul").empty();
  });
}

function mermaidEditorRender(textarea) {
  mermaid.render("theGraph", textarea.val(), (svgCode) => {
    $(".mermaid").html(svgCode);
  });

  $('.node').each(function() {
    elementHeightFix($(this));
  });

  mermaidRender();

  addEventContextMenu();
}

function createNode(elementText, elementName, elementClass, elementId, elementNextId, nodeType, moreInfoIcon) {
  let moreInfoCallback;
  let textToAppend = "";
  
  if ($(".editor textarea").val().trim() === "graph TD") {
    textToAppend = `\nC1(("${elementText}${moreInfoIcon}"))\n`;

    if ($("#add-more-info").is(":checked")) {
      moreInfoCallback = `click C1 call moreInfoCallback(${$("#more-info-id").val()})\n`;
      elementFunctions.push(moreInfoCallback);
      textToAppend = `${textToAppend}${moreInfoCallback}`;
    }
  }

  if (nodeType === "losango") {
    const indexRs = index++;
    const indexRn = index++;
    const optionChildClass =
      elementName.includes("RS") || elementName.includes("RN")
        ? `:::option-child-${elementId}`
        : elementClass;

    
    textToAppend =
      `${elementName} --> L${elementNextId}{"${elementText}${moreInfoIcon}"}${optionChildClass}\n` +
      `L${elementNextId} --> RS${indexRs}(["Sim"])${optionChildClass}\n` +
      `RS${indexRs}:::option-dad-${indexRs}\n` +
      `L${elementNextId} --> RN${indexRn}(["Não"])${optionChildClass}\n` +
      `RN${indexRn}:::option-dad-${indexRn}\n`;
    
      
    fatherOptions.push(`L${elementNextId} - ${elementText}`);  

    if ($("#add-more-info").is(":checked")) {
      moreInfoCallback = `click L${elementNextId} call moreInfoCallback(${$(
        "#more-info-id"
      ).val()})\n`;
      elementFunctions.push(moreInfoCallback);
      textToAppend = `${textToAppend}${moreInfoCallback}`;
    }
  }

  if (nodeType === "retangulo") {
    const optionChildClass =
      elementName.includes("RS") || elementName.includes("RN")
        ? `:::option-child-${elementId}`
        : elementClass;

    textToAppend = `${elementName} --> R${elementNextId}("${elementText}${moreInfoIcon}")${optionChildClass}\n`;
    
    fatherOptions.push(`R${elementNextId} - ${elementText}`);

    if ($("#add-more-info").is(":checked")) {
      moreInfoCallback = `click R${elementNextId} call moreInfoCallback(${$(
        "#more-info-id"
      ).val()})\n`;
      elementFunctions.push(moreInfoCallback);
      textToAppend = `${textToAppend}${moreInfoCallback}`;
    }
  }

  $(".editor textarea").val($(".editor textarea").val() +
    $(".editor textarea").html(textToAppend).text()
  );
}

function updateNode(elementText, elementName, moreInfoIcon) {
  const editorText = $(".editor textarea").val();
  const editorRows = editorText.split("\n");
  let positionElementRow;
  let elementEditable;
  let elementWrap;
  let moreInfoCallback;
  let textToAppend = "";

  for (let row of editorRows) {
    if (row.includes(elementName)) {
      positionElementRow = row.indexOf(elementName);
      elementEditable = row.substring(positionElementRow, row.length);
      elementWrap = row.replace(/"([^"]+)"/g, "");

      for (let elementFunction of elementFunctions) {
        if (
          elementFunction.includes(elementName) &&
          row.includes("moreInfoCallback")
        ) {
          $(".editor textarea").val(
            $(".editor textarea")
              .html($(".editor textarea").html().replace(row, ""))
              .text()
          );
        }
      }

      if (
        !elementEditable.includes("-->") &&
        (elementEditable.includes('("') ||
          elementEditable.includes('(("') ||
          elementEditable.includes('{"'))
      ) {
        if (elementEditable.includes('(("')) {
          textToAppend = $(".editor textarea")
            .val()
            .replace(
              row,
              elementWrap.replace("((", `(("${elementText}${moreInfoIcon}"`)
            );
        } else if (elementEditable.includes('("')) {
          textToAppend = $(".editor textarea")
            .val()
            .replace(
              row,
              elementWrap.replace("(", `("${elementText}${moreInfoIcon}"`)
            );
        } else if (elementEditable.includes('{"')) {
          textToAppend = $(".editor textarea")
            .val()
            .replace(
              row,
              elementWrap.replace("{", `{"${elementText}${moreInfoIcon}"`)
            );
        }

        if ($("#add-more-info").is(":checked")) {
          moreInfoCallback = `click ${elementName} call moreInfoCallback(${$(
            "#more-info-id"
          ).val()})\n`;
          elementFunctions.push(moreInfoCallback);
          textToAppend = `${textToAppend}${moreInfoCallback}`;
        }

        $(".editor textarea").val(
          $(".editor textarea").html(textToAppend).text()
        );
      }
    }
  }
}

jQuery(() => {
  $(".mermaid").html(
    '<div class="preload-mermaid">Digite o texto do primeiro elemento para renderizar o fluxograma.</div>'
  );

  $(".element-text").css("display", "flex").fadeIn(200).find("textarea").focus();

  $(".editor textarea").keyup(function () {
    const textarea = $(this);
    clearTimeout(timer);
    timer = setTimeout(() => {
      mermaidEditorRender(textarea);
    }, 2000);
  });

  $(".element-text .send-element-text").click(function () {
    const action = $(this).attr("action");
    const nodeType = $(this).attr("option-type");
    const elementName = $(this).attr("option-element-name");
    const elementClass = $(this).attr("option-element-class");
    const elementId = parseInt($(this).attr("option-element-id"));
    const elementNextId = index++;
    const elementText = $(".element-text textarea").val().replace(/\n$/, "").trim().replace(/(?:\r\n|\r|\n)/g, "<br>");
    let moreInfoIcon = "";

    if ($("#add-more-info").is(":checked")) {
      moreInfoIcon = "&lt;span class='mais-informacoes'&gt;&lt;br&gt;&lt;br&gt;#9432;&lt;/span&gt;";
    }

    if ($(".element-text textarea").val().length === 0) {
      alert("O texto do elemento não pode ser vazio!");
      return false;
    }

    if ($("#more-info-id").is(":visible")) {
      if ($("#more-info-id").val().length === 0) {
        alert("O campo Id Mais Informações é obrigatório!");
        return false;
      }
    }

    if (action === "create") {
      createNode(elementText, elementName, elementClass, elementId, elementNextId, nodeType, moreInfoIcon);
    } else if (action === "update") {
      updateNode(elementText, elementName, moreInfoIcon);
    }

    $(".element-text").fadeOut(200);
    $("#add-more-info").prop("checked", false);
    $("#more-info-id").val("").fadeOut(200);

    mermaidEditorRender($(".editor textarea"));
  });

  $(".options-dropdown").on("click", ".build-option", function (e) {
    console.log("clicou no losango");
    e.preventDefault();
    const [type, elementName, elementId, elementClass] = $(this)
      .attr("id")
      .split("|");
    showTextBox("create", type, elementName, elementId, elementClass, "");
  });

  $(".options-dropdown").on("click", ".build-option-add-pai", function (e) {
    console.log("clicou no adicionar paiii");
    e.preventDefault();
    const type = 'tipo';
    const elementName = 'elementName';
    const elementId = 'elementId';
    const elementClass = 'elementClass';
    showTextBox("addFather", type, elementName, elementId, elementClass, "");

  });

  // $(".element-text").on("keydown", "textarea, #more-info-id", (e) => {
  //   if (e.keyCode === 27 && $("g.nodes .node").length > 0) {
  //     $(".element-text").fadeOut(200);
  //   }
  // });

  $("body").on("keydown", function (e) {
    if (e.keyCode === 27 && $("g.nodes .node").length > 0) {
      $(".element-text, .element-text-adicionar-pai").fadeOut(200);
    }
  });

  $(".options-dropdown").on("click", ".build-option-add-pai", function (e) {
    console.log("clicou no adicionar paiii");
    e.preventDefault();
    const type = 'tipo';
    const elementName = 'elementName';
    const elementId = 'elementId';
    const elementClass = 'elementClass';
    showTextBox("addFather", type, elementName, elementId, elementClass, "");

  });


  $(window).bind('beforeunload', function(){
      return 'Você deseja realmente sair da página?\nAs alterações que você fez serão perdidas.';
  });

  $("#add-more-info").click(function () {
    if ($(this).is(":checked")) {
      $("#more-info-id").fadeIn(200);
    } else {
      $("#more-info-id").fadeOut(200);
    }
  });

  $("#more-info-id").keyup(function (e) {
    $(this).val(
      $(this)
        .val()
        .replace(/[^0-9]/gi, "")
    );
  });

  $(".options-dropdown").on("click", ".element-menu", function () {
    const elementName = elementClicked.attr("id").split("-")[1];
    const elementNumber = parseInt(elementName.replace(/[^0-9]/gi, ""));
    let textToAppend = "";

    if ($(this).hasPartialAttr("remove")) {
      const editorText = $(".editor textarea").val();
      const editorRows = editorText.split("\n");

      for (const row of editorRows) {
        if (
          !row.includes(elementName) &&
          !row.includes(`RS${elementNumber + 1}`) &&
          !row.includes(`RN${elementNumber + 2}`)
        ) {
          textToAppend = `${textToAppend}${
            textToAppend !== "" ? "\n" : ""
          }${row}`;
        }
      }

      $(".editor textarea").val(textToAppend).keyup();
      $(".options-dropdown").mouseleave();
    }
  });

  $(".option-text-element-wrap input").change(function(e) {
    if($(this).val() === "text") {
      $(".element-text textarea")
        .attr("placeholder", "Digite aqui o texto do elemento...")
        .removeClass("list")
        .addClass("text")
        .val("");
    } else if($(this).val() === "list") {
      $(".element-text textarea")
        .attr("placeholder", "Digite aqui a lista de itens para o elemento... Cada vez que você teclar ENTER será um item novo na lista!")
        .removeClass("text")
        .addClass("list")
        .val("");
    }
  });

  $(".element-text-symbols").on("click", function () {
    insertAtCaret("element-textarea", $(this).val());
  });
});
